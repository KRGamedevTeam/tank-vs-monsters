﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{
    [HideInInspector]
    public Transform target;

    public int damage = 20;
    NavMeshAgent m_Agent;

    Animation animation;

    float speed = 1;

    void Start()
    {
        animation = GetComponent<Animation>();
        
        
    }

    public void InitNavigation(float speed, float rotateSpeed)
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Agent.speed = speed;
        m_Agent.angularSpeed = rotateSpeed;
        if (m_Agent.isOnNavMesh)
            m_Agent.destination = target.position;
    }
    
    void Update()
    {
        animation.Play("walk");
        if (animation.IsPlaying("walk"))
            animation.CrossFade("idle01", 0.3f);


        if (m_Agent.isOnNavMesh)
            m_Agent.destination = target.position;
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            var playerHP = collision.collider.GetComponent<TankHealth>();
            playerHP.TakeDamage(damage);

            if (CheckAniClip("attack01") == false)
                return;
            animation.CrossFade("attack01", 0.2f);
            animation.GetComponent<Animation>().CrossFadeQueued("idle01");
        }
    }
    bool CheckAniClip(string clipname)
    {
        if (animation.GetClip(clipname) == null)
            return false;
        else if (animation.GetClip(clipname) != null)
            return true;

        return false;
    }
}
