﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigInstance : SingletonBehaviour<ConfigInstance>
{
    public Config config;

    [ContextMenu("Reset Progresss")]
    public void ResetProgress()
    {
        PlayerPrefs.DeleteAll();
    }

    private void OnDestroy()
    {
        Instance = null;
    }
}
