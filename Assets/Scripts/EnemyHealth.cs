﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int hp;
    public int armor;

    public void Init(int hp, int armor)
    {
        this.hp = hp;
        this.armor = armor;
    }

    public void TakeDamage(int damage)
    {
        var nonBlockDamage =Mathf.Abs( damage * (1 - armor/100));

        hp -= nonBlockDamage;
        if (hp <= 0)
        {
            hp = 0;
            Spawner.Instance.enemyCount--;
            Destroy(gameObject);
        }
    }
}
