﻿using UnityEditor;
using UnityEngine;

namespace Submodules.MaverickUtils.Config.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(MonoBehaviour), true)]
    public class MonoBehaviourEditor : UnityEditor.Editor
    {
    }
}