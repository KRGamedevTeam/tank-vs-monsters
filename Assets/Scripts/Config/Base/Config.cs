using System;
using UnityEngine;


public interface IConfig
{
}

[CreateAssetMenu(fileName = "Config", menuName = "Config", order = 1)]
[Serializable]
public class Config : ScriptableObject, IConfig
{
    [Header("Player Settings")]
    [Space]
    public float tankSpeed;
    public float tankRotateSpeed;
    public int tankHp;
    public int tankArmor;

    [Header("Enemy Settings")]
    [Space]
    public EnemyConfig[] enemys;
    public float spawnDistance;
    public float spawnTime;

    public int maxEnemy = 10;
}
