﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed=3;
    public int damage= 10;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(transform.forward * rb.mass * speed, ForceMode.Impulse);

        if (rb.velocity.magnitude > speed)
            rb.velocity = rb.velocity.normalized * speed;

        if (transform.position.x > 80 || transform.position.x < -80 ||
            transform.position.z > 80 || transform.position.z < -80)
            Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            return;

        if (other.GetComponent<Bullet>())
            return;

        if(other.CompareTag("Enemy"))
        {
            var en = other.GetComponent<EnemyHealth>();
            if (en)
                en.TakeDamage(damage);
        }

        Destroy(gameObject);
    }
}
