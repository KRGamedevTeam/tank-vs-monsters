﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyConfig 
{
    public GameObject enemyPrefab;
    public int enemyArmor;
    public int enemyHP;
    public int damage = 20;
    public float enemySpeed;
    public float enemyRotateSpeed;
}
