﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManager : MonoBehaviour
{
    public TankMove mover;
    public TankWeapon weapon;


    void Update()
    {
        if (!mover)
            return;

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            mover.RotateWheels(1, -1, 1, 1);
            mover.Move(1);

        } 
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            mover.RotateWheels(-1, 1, -1, -1);
            mover.Move(-1);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            mover.RotateWheels(1, 1, 1, -1);
            mover.Rotate(-1);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            mover.RotateWheels(-1, -1, -1, 1);
            mover.Rotate(1);
        }

        if (!weapon)
            return;

        if (Input.GetKeyDown(KeyCode.X))
        {
            weapon.Shoot();            
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            weapon.ChangeBullet(-1);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            weapon.ChangeBullet(1);
        }
    }
}
