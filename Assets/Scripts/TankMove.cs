﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMove : SingletonBehaviour<TankMove>
{
    public GameObject[] LeftWheels;
    public GameObject[] RightWheels;

    public Renderer LeftTrack;
    public Renderer RightTrack;

    public float wheelsSpeed = 2f;
    public float tracksSpeed = 2f;
    public float forwardSpeed = 1f;
    public float rotateSpeed = 1f;

    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        forwardSpeed = ConfigInstance.Instance.config.tankSpeed;
        rotateSpeed = ConfigInstance.Instance.config.tankRotateSpeed;
    }

    
    public void Move(float forward)
    {
        rb.AddForce(transform.forward * rb.mass * forwardSpeed * forward, ForceMode.Impulse);

        if (rb.velocity.magnitude > forwardSpeed)
            rb.velocity = rb.velocity.normalized * forwardSpeed;
    }

    public void Rotate(float dir)
    {
        rb.angularVelocity = rb.transform.up * dir * rotateSpeed * Time.deltaTime;
    }

    public void RotateWheels(float leftWheel, float rightWheel, float leftTrack, float rightTrack)
    {
        foreach (GameObject wheelL in LeftWheels)
        {
            wheelL.transform.Rotate(Vector3.right*wheelsSpeed*leftWheel);
        }

        foreach (GameObject wheelR in RightWheels)
        {
            wheelR.transform.Rotate(Vector3.right * wheelsSpeed * rightWheel);
        }

        LeftTrack.material.mainTextureOffset += Vector2.up * Time.deltaTime * tracksSpeed * leftTrack;
        RightTrack.material.mainTextureOffset += Vector2.up * Time.deltaTime * tracksSpeed * rightTrack;

    }
}
