﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : SingletonBehaviour<Spawner>
{
    public int enemyCount = 0;
    public Transform player;
    public LayerMask mask;

    float spawnDistance;
    float spawnTime;

    int maxEnemy = 10;

    private void Start()
    {
        if (player == null)
            player = GameObject.FindObjectOfType<TankMove>().transform;

        enemyCount = 0;

        spawnDistance = ConfigInstance.Instance.config.spawnDistance;
        spawnTime = ConfigInstance.Instance.config.spawnTime;
        maxEnemy = ConfigInstance.Instance.config.maxEnemy;
    }

    private void Update()
    {
        if (spawnTime > 0)
            spawnTime -= Time.deltaTime;
        else
        {
            if (enemyCount < maxEnemy)
            {

                Spawn();

                spawnTime = ConfigInstance.Instance.config.spawnTime;
            }
        }
    }

    Vector3 GetPos(Vector2 distance)
    {
        Vector3 spawnPos;
        bool empty = false;
        int i = 0;
        do
        {
            spawnPos = player.position + Vector3.forward * (Random.Range(distance.x-2, distance.y+2)
                *(Random.value>0.5f?1:-1))
                + Vector3.right * (Random.Range(distance.x-2, distance.y+2)
                * (Random.value > 0.5f ? 1 : -1));
            spawnPos.y = 0;
            Ray ray = new Ray(new Vector3(spawnPos.x,-2, spawnPos.z), Vector3.down);
            empty = Physics.Linecast(new Vector3(spawnPos.x, 35, spawnPos.z), new Vector3(spawnPos.x, -1, spawnPos.z),mask);
            i++;
        } while (empty);

        return spawnPos;
    }

    public void Spawn()
    {
        if (!player)
            return;

        var spawnPos = GetPos(Vector2.one* spawnDistance);
        var enemy = ConfigInstance.Instance.config.enemys[Random.Range(0, ConfigInstance.Instance.config.enemys.Length)];
        Transform go = Instantiate(enemy.enemyPrefab.transform, spawnPos,Quaternion.identity) as Transform;

        float y = go.position.y;

        go.eulerAngles = Vector3.up * Random.Range(0, 360);
        go.eulerAngles += Vector3.up * 180;

        var en = go.GetComponent<EnemyMove>();
        if (en)
        {
            en.target = player;
            en.damage = enemy.damage;
            en.InitNavigation(enemy.enemySpeed, enemy.enemyRotateSpeed);
        }
        var enHP = go.GetComponent<EnemyHealth>();
        if(enHP)
        {
            enHP.Init( enemy.enemyHP,enemy.enemyArmor);
        }

        enemyCount++;
    }

    private void OnDestroy()
    {
        Instance = null;
    }
}
