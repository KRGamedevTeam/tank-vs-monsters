﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWeapon : MonoBehaviour
{
    public GameObject[] bullets;
    public Transform bulletPos;

    private int currentBullet;

    public void ChangeBullet(int index)
    {
        currentBullet += index;
        if (currentBullet >= bullets.Length)
            currentBullet = 0;
        if (currentBullet < 0)
            currentBullet = bullets.Length - 1;
    }

    public void Shoot()
    {
        Instantiate(bullets[currentBullet], bulletPos.position, bulletPos.rotation);
    }
}
