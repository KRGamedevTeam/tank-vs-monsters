using UnityEngine;

    public class PIDStabilizer : MonoBehaviour
    {
        public float frequency = 1f;
        public float damping = 0.1f;
        public Rigidbody rb;
        private Quaternion desiredRotation;
        public Vector3 desiredAngles;
        public bool active = false;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            if(!active) return;
           
          


          var  fwd = transform.forward;
            fwd.y = 0;
            fwd.Normalize();
            desiredRotation = Quaternion.LookRotation(fwd);
          //  desiredRotation = Quaternion.AngleAxis(transform.localEulerAngles.y, Vector3.up);
         //   desiredRotation = Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.x);
            Debug.DrawLine(transform.position,transform.position+desiredRotation*Vector3.forward*5f,Color.red);
            // desiredAngles = desiredRotation.eulerAngles;
            var kp = (6f*frequency)*(6f*frequency)* 0.25f;
            var kd = 4.5f*frequency*damping;
           
            
        float dt = Time.fixedDeltaTime;
        float g = 1 / (1 + kd * dt + kp * dt * dt);
        float ksg = kp * g;
        float kdg = (kd + kp * dt) * g;
        Vector3 x;
        float xMag;
        Quaternion q = Quaternion.FromToRotation(transform.forward,fwd);  //* Quaternion.Inverse(transform.rotation);
        q.ToAngleAxis (out xMag, out x);
        x.Normalize ();
        x *= Mathf.Deg2Rad;
        Vector3 pidv = kp * x * xMag - kd * rb.angularVelocity;
        Quaternion rotInertia2World = rb.inertiaTensorRotation * transform.rotation;
        pidv = Quaternion.Inverse(rotInertia2World) * pidv;
        pidv.Scale(rb.inertiaTensor);
        pidv = rotInertia2World * pidv;
        pidv.y = 0;
        if(!float.IsNaN(pidv.x))
        rb.AddTorque(pidv);
//        if (rb.rotation.x < 0)
//        {
//            rb.AddTorque(transform.right*rb.mass);
//        }
//        else
//        {
//            rb.AddTorque(transform.right*-rb.mass);
//        }
//        
//        
//        if (rb.rotation.z < 0)
//        {
//            rb.AddTorque(transform.forward*rb.mass);
//        }
//        else
//        {
//            rb.AddTorque(transform.forward*-rb.mass);
//        }
       }
        
        
        public Vector3 ComputeTorque(Quaternion desiredRotation){
            //q will rotate from our current rotation to desired rotation
            Quaternion q = desiredRotation * Quaternion.Inverse(transform.rotation);
            //convert to angle axis representation so we can do math with angular velocity
            Vector3 x;
            float xMag;
            q.ToAngleAxis (out xMag, out x);
            x.Normalize ();
            //w is the angular velocity we need to achieve
            Vector3 w = x * xMag * Mathf.Deg2Rad / Time.fixedDeltaTime;
            w -= rb.angularVelocity;
            //to multiply with inertia tensor local then rotationTensor coords
            Vector3 wl = transform.InverseTransformDirection (w);
            Vector3 Tl;
            Vector3 wll = wl;
            wll = rb.inertiaTensorRotation * wll;
            wll.Scale(rb.inertiaTensor);
            Tl = Quaternion.Inverse(rb.inertiaTensorRotation) * wll;
            Vector3 T = transform.TransformDirection (Tl);
            return T;
        }
    }