﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    public int hp;
    public int armor;

    public Text hpText;

    private void Start()
    {
        hp = ConfigInstance.Instance.config.tankHp;
        armor = ConfigInstance.Instance.config.tankArmor;

        hpText.text = hp.ToString();
    }

    public void TakeDamage(int damage)
    {
        var nonBlockDamage = Mathf.Abs( damage * (1 -  armor/100));

        hp -=  nonBlockDamage;
        if(hp<=0)
        {
            hp = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }


        hpText.text = hp.ToString();
    }
}
